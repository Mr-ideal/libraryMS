from django.contrib import admin

# Register your models here.

from .models import userMessage  
from .models import bookMessage  
from .models import history  

admin.site.register(userMessage)
admin.site.register(bookMessage)
admin.site.register(history)
