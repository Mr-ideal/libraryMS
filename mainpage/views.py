# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response

from  django.conf import settings
from mainpage.models import userMessage  
from mainpage.models import bookMessage  
from mainpage.models import history  

import os
import random

# Create your views here.


def index(request):     #设置cookies  
    response= render_to_response('index.html') 
    if "user_cookie"  not in request.COOKIES or 'user_name' not in request.COOKIES:
        strlist="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
        usermess=''
        for i in range(20):
            usermess=usermess+strlist[random.randint(0,len(strlist)-1)]
        #print(usermess)
        response.set_cookie('user_cookie',usermess,365*24*60*60*1)#设置
        response.set_cookie('user_name',bytes("登录", 'utf-8').decode('ISO-8859-1'),365*24*60*60*1)#设置用户名
    return response  

def initBooks():
    books=bookMessage.objects.all().values('bookCode') 
    if not books.exists():
        readBookToSQL() #初始化书籍


def readBookToSQL():    #读取图书到数据库
    booksfile= open(os.path.join(settings.BASE_DIR, "books.txt"), 'r')
    bookclass=""
    flag=0
    for line in booksfile:
        if len(line.strip())>1:
            if  line.strip()=="####":
                flag=1
            elif flag==1:
                bookclass=line.strip()
                flag=0
            else :
                pro=bookMessage.objects.create( #存入数据库
                    bookName = line.strip(),   #书名
                    bookClass=bookclass ,   #图书类别
                    whereIs = "本部门",   #典藏部门
                    status = "在馆",   #流通状态
                    outTime = 30, #应还期限
                )
                #print(line.strip(),bookclass)
                #pro.save()
    booksfile.close()
def getAllBooks():
    books=bookMessage.objects.all().values_list('bookCode','bookName','bookClass','whereIs','status','outTime') 
    #print(books)
    
    return books
    
def initPage(request):    
    
    initBooks()
    #return HttpResponse("hello")
    context=dict(
        errormessage="",
        uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8'),
        books=getAllBooks(),
    )
    response= render_to_response('initPage.html',context) 
    return response  
    
def login(request):
    return render_to_response('login.html')
def logon(request):
    return render_to_response('logon.html')
    
from django.views.decorators.csrf import csrf_exempt	#这两句是让下面的函数接受form表单的上传文件不会出CSRF的错误，在html 的form中加一句 {% csrf_token %}

@csrf_exempt
def logonback(request):  #注册
    uname=request.POST['username']
    pword=request.POST['password']   
    if 'logon' in request.POST:     #注册 
        if len(uname)<4 or len(pword)<4 :
            context={
                'errormessage':"用户名或密码长度不能小于等于4！",
                }
            return render_to_response('logon.html',context)
   
        if userMessage.objects.filter(userName=uname).count()>0:
            context={
            'errormessage':"用户名已注册！",
            }
            response= render_to_response('logon.html',context) 
            return response #注册失败'denglu'.encode('utf-8')
        
        #注册成功
        print(request.POST['sex'] )
        pro=userMessage.objects.create(#存入数据库
			userName = uname,
			passWord = pword,
			realName = request.POST['realname']   ,  #姓名
            sex = request.POST['sex']      ,   #性别
            userClass="读者"   ,  #用户类别（管理员or读者）
            callMeth =request.POST['email']    ,  #邮箱--
            address=request.POST['address']   , #地址
		)
        #pro.save()
        response= render_to_response('login.html')
        return response
    
@csrf_exempt
def loginback(request):     #登录
    uName=request.POST['username']
    pWord=request.POST['password']
    if len(uName)<1 or len(pWord)<1 :
        return render_to_response('login.html')
    umess = userMessage.objects.filter(userName=uName).values_list("passWord")
    for data in umess:
        if  pWord==data[0]:
            #return HttpResponse("helloworld")
            context=dict(
                uname=uName,
                errormessage="",
                books=getAllBooks(),
            )
            response= render_to_response('initPage.html',context) 
            response.set_cookie('user_name',bytes(uName, 'utf-8').decode('ISO-8859-1'),24*60*60)#设置用户名,1天
            #return HttpResponse("helloworld")
            return response #登录成功
    
    #登录失败 
    context={
    'errormessage':"用户名或密码错误请重新输入！",
    }
    response= render_to_response('login.html',context)
    return response
    
def showUserMess(request):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    selfmess=userMessage.objects.filter(userName=uname).values_list("userName","passWord","realName","sex","userClass","callMeth","address")
    context=dict(
    errormessage="",
    selfMess=list(selfmess)[0],
    )
    return render_to_response('selfmess.html',context)
  
@csrf_exempt  
def changemess(request):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    userMessage.objects.filter(userName=uname).update(userName=request.POST['username'],passWord=request.POST['password'],realName=request.POST['realname'],sex=request.POST['sex'],callMeth=request.POST['email'],address=request.POST['address'])
    selfmess=userMessage.objects.filter(userName=uname).values_list("userName","passWord","realName","sex","userClass","callMeth","address")
    context=dict(
        errormessage="保存成功",
        selfMess=list(selfmess)[0],
    )
    return render_to_response('selfmess.html',context)
   
   
def delUser(request):
    context=dict(
        errormessage="",
        uname="登录",
        books=getAllBooks(),
    )
    response= render_to_response('initPage.html',context) 
    response.set_cookie('user_name',bytes("登录", 'utf-8').decode('ISO-8859-1'),365*24*60*60*1)#设置用户名
    return response  
    
def outHistory(request):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    outhistory=history.objects.filter(userName=uname).values_list("userName","bookCode","bookName","activeClass","activeTime")
    if outhistory.exists():
        outhistory=list(outhistory)
        tempHistory=[]
        for k in outhistory:
            temp=list(k)
            temp[4]=temp[4].strftime('%y-%m-%d %I:%M:%S %p')
            if temp[3]!="在借中":
                tempHistory.append(temp)
        outhistory=tempHistory
        print(outhistory)
        
    else:
        outhistory=""
    context=dict(
        errormessage="",
        outHistory=outhistory,
    )
    return render_to_response('outHistory.html',context)
    
def usingBook(request):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    outhistory=history.objects.filter(userName=uname,).values_list("userName","bookCode","bookName","activeClass","activeTime")
    if outhistory.exists():
        outhistory=list(outhistory)
        tempHistory=[]
        for k in outhistory:
            temp=list(k)
            temp[4]=temp[4].strftime('%y-%m-%d %I:%M:%S %p')
            if temp[3]=="在借中":
                tempHistory.append(temp)
        outhistory=tempHistory
        print(outhistory)
        
    else:
        outhistory=""
    context=dict(
        errormessage="",
        outHistory=outhistory,
    )
    return render_to_response('usingBook.html',context)
    
def getBook(request ,bcode):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    if history.objects.filter(userName=uname,activeClass="在借中").count()<20 and uname!="登录":
        books=bookMessage.objects.filter(bookCode=bcode).values_list('status') 

        emessage=""
        if books.exists() and list(list(books)[0])[0]=="在馆":
            bookMessage.objects.filter(bookCode=bcode).update(status="外借")
            pro=history.objects.create(#存入数据库
			    userName=uname,   #借阅人
                bookCode = bcode,   #图书编号
                bookName = list(list(bookMessage.objects.filter(bookCode=bcode).values_list('bookName'))[0])[0],    #书名
                activeClass="借书",
		    )
            #pro.save()
            pro=history.objects.create(#存入数据库
			    userName=uname,   #借阅人
                bookCode = bcode,   #图书编号
                bookName = list(list(bookMessage.objects.filter(bookCode=bcode).values_list('bookName'))[0])[0],    #书名
                activeClass="在借中",
		    )
            #pro.save()
        
        else:
            emessage="该书已借出，请借阅其他图书"
    elif uname=="登录":
            emessage="请先登录"
    else:
        emessage="你的借书数量超过20,请还书后再借"
    context=dict(
    errormessage=emessage,
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8'),
    books=getAllBooks(),
    )
    response= render_to_response('initPage.html',context) 
    return response
def returnBook(request ,bcode):
    uname=bytes(request.COOKIES["user_name"], 'ISO-8859-1').decode('utf-8')
    bookMessage.objects.filter(bookCode=bcode).update(status="在馆")
    books=history.objects.filter(userName=uname,bookCode=bcode,activeClass="在借中").update(activeClass="还书")

    return usingBook(request)
    
    
