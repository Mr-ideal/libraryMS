from django.conf.urls import url

from . import views

app_name='[mainpage]'

urlpatterns = [
    url(r'^$', views.index, name= 'index'),
    url(r'^initPage$', views.initPage, name= 'initPage'),
     url(r'^login$', views.login, name= 'login'),
    url(r'^logon$', views.logon, name= 'logon'),
    url(r'^loginback$', views.loginback, name= 'loginback'),
    url(r'^logonback$', views.logonback, name= 'logonback'),
    url(r'^showUserMess$', views.showUserMess, name= 'showUserMess'),
    url(r'^changemess$', views.changemess, name= 'changemess'),
    url(r'^delUser$', views.delUser, name= 'delUser'),
    url(r'^outHistory$', views.outHistory, name= 'outHistory'),
    url(r'^usingBook$', views.usingBook, name= 'usingBook'),
    url(r'^getBook(.*)', views.getBook, name= 'getBook'),
    url(r'^returnBook(.*)', views.returnBook, name= 'returnBook'),
    
    
    #url(r'^download(.*)', views.download, name= 'download'),
]

    


"""

电脑上运行、连接MySQL数据库（初始100本书）
1.管理员登录。管理员个人信息（编号、姓名、性别、联系方式、住址），管理图书信息。
2.(这个不需要了)馆藏图书检索。读者需要根据键入词及检索方式检索所需结果。检索结果呈现格式有书目详细信息（图书编号、书名、作者、内容简介、主题、出版社、）、馆内流通信息（索书号、登录号、典藏部门、应还期限）
3.个人信息查询。读者输入用户名及密码，登录个人中心。账号信息（用户编号、用户姓名、性别、住址、到期时间、用户类型）、借书信息（图书编号、典藏部门、流通状态、应还期限、续借）、借阅历史等
4.借书信息。显示读者所借阅图书的详细信息，书名、图书编号典藏部门、流通状态、应还日期及需要续借的功能，时长默认一个月。
借阅历史。显示读者借阅历史。包括书名、图书编号、类型（借书or还书）和更改日期
5.个性化图书推荐。利用读者借阅图书信息、检索图书信息，向读者推荐相似图书。

"""
